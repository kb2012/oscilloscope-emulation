﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;

namespace OscilloscopeEmulation
{
    /// <summary>
    /// Interaction logic for LogoAnimationWindow.xaml
    /// </summary>
    public partial class LogoAnimationWindow : Window
    {
        private Animation _animation;
        public LogoAnimationWindow()
        {
            InitializeComponent();
        }

        public void ShowLogo(Window generalWindow)
        {
            Grid.SetZIndex(PassAnimationTextBlock, 1);
            _animation = new Animation(LogoAnimation, this, generalWindow);
            _animation.Animate();
            var opacityAnimation = new DoubleAnimation
            {
                From = 0,
                To = 1,
                Duration = new Duration(TimeSpan.FromMilliseconds(1500))
            };
            PassAnimationTextBlock.BeginAnimation(OpacityProperty, opacityAnimation);
        }

        private void UIElement_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            _animation.Stop();
        }
    }
}
