﻿using System;

namespace OscilloscopeEmulation
{
    public class Probabilities
    {
        //public void xd(string[] args)
        //{
        //    double lowLimit = -0.7; //Нижний уровень помехи
        //    double highLimit = 0.8; //Верхний уровень помехи
        //    double signalHeight = 1; //Высота сигнала, 1 В к примеру
        //    double signalZero = 0;
        //    double minLevelFor1 = 0.95; //Мин уровень единички, к примеру 0.7 В
        //    double maxLevelFor0 = 0.3; //Макс уровень нуля

        //    var m0 = signalZero + (lowLimit + highLimit) / 2;
        //    var m1 = signalHeight + (lowLimit + highLimit) / 2;
        //    var sd = (highLimit - lowLimit) / (3 + 3); //правило трех сигм

        //    var p0 = Fraspr(maxLevelFor0, m0, sd) - Fraspr(signalZero + lowLimit, m0, sd);
        //    var p1 = Fraspr(signalHeight + highLimit, m1, sd) - Fraspr(minLevelFor1, m1, sd);

        //    Console.WriteLine("Вероятность 1->0 " + p1);
        //    Console.WriteLine("Вероятность 0->1 " + p0);
        //    Console.ReadLine();
        //}

        public double Erf(double x)
        {
            // constants
            const double a1 = 0.254829592;
            const double a2 = -0.284496736;
            const double a3 = 1.421413741;
            const double a4 = -1.453152027;
            const double a5 = 1.061405429;
            const double p = 0.3275911;

            // Save the sign of x
            int sign = 1;
            if (x < 0)
                sign = -1;
            x = Math.Abs(x);

            // A&S formula 7.1.26
            double t = 1.0 / (1.0 + p * x);
            double y = 1.0 - (((((a5 * t + a4) * t) + a3) * t + a2) * t + a1) * t * Math.Exp(-x * x);

            return sign * y;
        }

        public double Fraspr(double x, double m, double sd)
        {
            var arg = (x - m) / Math.Sqrt(2 * sd * sd);
            return 1.0 / 2 * (1 + Erf(arg));
        }
    }
}
