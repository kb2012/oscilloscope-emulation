﻿using System;
using System.Collections.Generic;
using OscilloscopeEmulation.SignalSettingsPanels;

namespace OscilloscopeEmulation.Signals
{
    public class DiscretizedSignal : ISignal
    {
        public ISignal Signal { get; private set; }
        public double DiscretizationFrequency { get; private set; }
        public ISignalSettingsPanel SignalSettingsPanel { get; set; }

        public DiscretizedSignal(ISignal signal, double discretizationFrequency)
        {
            Signal = signal;
            DiscretizationFrequency = discretizationFrequency;
        }

        public IList<double> GetValuesList(int totalMSec, int totalEntries) //10 мсек и 1000 по умолчанию
        {
            var list = new List<double>();
            for (var i = 0; i < totalEntries; i++)
            {
                list.Add(GetValue(i, totalMSec, totalEntries));
            }
            return list;
        }

        public double GetValue(int i, int totalMSec, int totalEntries)
        {
            var msecPassed = i * (double)totalMSec / totalEntries;
            var period = 1.0/DiscretizationFrequency;
            var modMsec = msecPassed;
            //Цикл эмулирует mod (деление с остатком) для double
            while (modMsec >= period)
            {
                modMsec -= period;
            }

            const double tolerance = 0.001;
            if (Math.Abs(modMsec) < tolerance)
            {
                return Signal.GetValue(i, totalMSec, totalEntries);
            }
            return 0;
        }

        public string GetName()
        {
            return string.Format("Дискретизированный с частотой {0} кГц", DiscretizationFrequency);
        }

        public ISignalSettingsPanel GetSignalSettingsPanel()
        {
            //TODO
            return null;
        }

        public void CloneSignal(ISignal signal)
        {
            throw new NotImplementedException();
        }
    }
}
