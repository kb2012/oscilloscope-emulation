﻿using System.Collections.Generic;
using OscilloscopeEmulation.SignalSettingsPanels;

namespace OscilloscopeEmulation.Signals
{
    public interface ISignal
    {
        IList<double> GetValuesList(int totalMSec, int totalEntries);
        double GetValue(int i, int totalMSec, int totalEntries);
        string GetName();
        ISignalSettingsPanel GetSignalSettingsPanel();
        void CloneSignal(ISignal signal);
    }
}
