﻿using System;
using System.Collections.Generic;
using OscilloscopeEmulation.SignalSettingsPanels;

namespace OscilloscopeEmulation.Signals
{
    public class LowFilteredSignal : ISignal
    {
        public ISignal Signal { get; private set; }
        public ISignalSettingsPanel SignalSettingsPanel { get; set; }

        public int N { get; private set; } //Длина фильтра
        public double Fd { get; private set; } //Частота дискретизации входных данных
        public double Fs { get; private set; } //Частота полосы пропускания
        public double Fx { get; private set; } //Частота полосы затухания

        public static double Multiplier = 1.0;


        double[] H; //Импульсная характеристика фильтра

        public LowFilteredSignal(ISignal signal)
        {
            Signal = signal;
            H = getImpulseResponse();
        }

        public LowFilteredSignal(ISignal signal, int n, double fd, double fs, double fx)
        {
            Signal = signal;

            N = n;
            Fd = fd * 1000; // В кГц
            Fs = fs * 1000;
            Fx = fx * 1000;

            H = getImpulseResponse();
        }

        public IList<double> GetValuesList(int totalMSec, int totalEntries)
        {
            var list = new List<double>();
            for (var i = 0; i < totalEntries; i++)
            {
                list.Add(GetValue(i, totalMSec, totalEntries));
            }
            return list;
        }

        public double GetValue(int i, int totalMSec, int totalEntries)
        {
            //Фильтрация входных данных
            var result = 0.0;
            for (var j = 0; j < N - 1; j++) // та самая формула фильтра
            {
                if (i >= j)
                    result += H[j] * Signal.GetValue(i - j, totalMSec, totalEntries);
            }
            return result;
        }


        public string GetName()
        {
            return string.Format("ФНЧ");
        }

        public ISignalSettingsPanel GetSignalSettingsPanel()
        {
            //TODO 
            return null;
        }

        public void CloneSignal(ISignal signal)
        {

            N = (signal as LowFilteredSignal).N;
            Fd = (signal as LowFilteredSignal).Fd;
            Fs = (signal as LowFilteredSignal).Fs;
            Fx = (signal as LowFilteredSignal).Fx;
        }

        private double[] getImpulseResponse()
        {
            var H = new double[N];
            double[] H_id = new double[N]; //Идеальная импульсная характеристика
            double[] W = new double[N]; //Весовая функция

            //Расчет импульсной характеристики фильтра
            double Fc = (Fs + Fx) / (2 * Fd);

            for (int i = 0; i < N; i++)
            {
                if (i == 0) H_id[i] = 2 * Math.PI * Fc;
                else H_id[i] = Math.Sin(2 * Math.PI * Fc * i) / (Math.PI * i);
                // весовая функция Блекмена
                W[i] = 0.42 - 0.5 * Math.Cos((2 * Math.PI * i) / (N - 1)) + 0.08 * Math.Cos((4 * Math.PI * i) / (N - 1));
                H[i] = H_id[i] * W[i];
            }

            //Нормировка импульсной характеристики
            double SUM = 0;
            for (int i = 0; i < N; i++) SUM += H[i];
            for (int i = 0; i < N; i++) H[i] = (H[i] / SUM) * Multiplier;
            return H;
        }

    }
}