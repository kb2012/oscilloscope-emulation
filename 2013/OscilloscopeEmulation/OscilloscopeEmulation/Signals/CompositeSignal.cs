﻿using System.Collections.Generic;
using OscilloscopeEmulation.SignalSettingsPanels;

namespace OscilloscopeEmulation.Signals
{
    public class CompositeSignal : ISignal
    {
        public List<ISignal> Signals { get; private set; }
        public ISignalSettingsPanel SignalSettingsPanel { get; set; }
        public int ChannelNumber { get; set; }

        public CompositeSignal(int channelNumber)
        {
            Signals = new List<ISignal>();
            ChannelNumber = channelNumber;
        }
        public void Add(ISignal signal)
        {
            Signals.Add(signal);
        }

        public IList<double> GetValuesList(int totalMSec, int totalEntries) //10 мсек и 1000 по умолчанию
        {
            var list = new List<double>();
            for (var i = 0; i < totalEntries; i++)
            {
                list.Add(GetValue(i, totalMSec, totalEntries));
            }
            return list;
        }

        public double GetValue(int i, int totalMSec, int totalEntries)
        {
            double value = 0;
            foreach (var signal in Signals)
            {
                value += signal.GetValue(i, totalMSec, totalEntries);
            }
            return value;
        }

        public string GetName()
        {
            return string.Format("Составной сигнал");
        }

        public ISignalSettingsPanel GetSignalSettingsPanel()
        {
            if (SignalSettingsPanel == null)
                SignalSettingsPanel = new CompositSignalSettingsPanel();
            return SignalSettingsPanel;
        }

        public void CloneSignal(ISignal signal)
        {
            foreach (var currentSignal in Signals)
            {
                currentSignal.CloneSignal(signal);
            }
        }
    }
}
