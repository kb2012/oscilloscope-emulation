﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OscilloscopeEmulation.Signals
{
    class FNCh: ISignal
    {
        const int N = 20; //Длина фильтра
        const double Fd = 2000; //Частота дискретизации входных данных
        const double Fs = 20; //Частота полосы пропускания
        const double Fx = 50; //Частота полосы затухания

        public IList<double> GetValuesList(int totalMSec, int totalEntries)
        {
            var list = new List<double>();
            for (var i = 0; i < totalEntries; i++)
            {
                list.Add(GetValue(i, totalMSec, totalEntries));
            }
            return list;
        }

        public double GetValue(int _in, int totalMSec, int totalEntries)
        {
            double H = 0.0; //Импульсная характеристика фильтра
            double H_id; //Идеальная импульсная характеристика
            double W; //Весовая функция

            //Расчет импульсной характеристики фильтра
            double Fc = (Fs + Fx)/(2*Fd);

            for (int i = 0; i < N; i++)
            {
                if (i == 0) H_id = 2*Math.PI*Fc;
                else H_id = Math.Sin(2*Math.PI*Fc*i)/(Math.PI*i);
                // весовая функция Блекмена
                W = 0.42 - 0.5*Math.Cos((2*Math.PI*i)/(N - 1)) + 0.08*Math.Cos((4*Math.PI*i)/(N - 1));
                H = H_id*W;
            }

            //Фильтрация входных данных
            var _out = 0.0;
            for (var j = 0; j < N - 1; j++) // та самая формула фильтра
                _out += H*_in;
            return _out;
        }

        public string GetName()
        {
            return string.Format("");
        }
    }
}
