﻿using OscilloscopeEmulation.SignalSettingsPanels;

namespace OscilloscopeEmulation.Signals
{
    public class ConstantSignal: HarmonicSignal //Постоянный это по сути гармонический с частотой 0, нач фаза тоже 0.
    {
        public ConstantSignal(double amplitude):base(amplitude,0,0)
        {
             SignalSettingsPanel = new ConstantSignalSettingsPanel();
        }

        public override string ToString()
        {
            return string.Format("Константный сигнал: [Амплитуда: '{0}']", Amplitude);
        }
    }
}
