﻿using System;
using System.Collections.Generic;
using OscilloscopeEmulation.SignalSettingsPanels;

namespace OscilloscopeEmulation.Signals
{
    public class AmplitudeModulatedSignal : ISignal
    {
        public ISignal SourceSignal { get; set; }
        public ISignal CarrierSignal { get; set; }

        public AmplitudeModulatedSignal(ISignal sourceSignal, ISignal carrierSignal)
        {
            SourceSignal = sourceSignal;
            CarrierSignal = carrierSignal;
        }

        public IList<double> GetValuesList(int totalMSec, int totalEntries)
        {
            var list = new List<double>();
            for (var i = 0; i < totalEntries; i++)
            {
                list.Add(GetValue(i, totalMSec, totalEntries));
            }
            return list;
        }

        public double GetValue(int i, int totalMSec, int totalEntries)
        {
            return SourceSignal.GetValue(i, totalMSec, totalEntries) * CarrierSignal.GetValue(i, totalMSec, totalEntries);
        }

        public string GetName()
        {
            return "";
        }

        public ISignalSettingsPanel GetSignalSettingsPanel()
        {
            //TODO
            return null;
        }

        public void CloneSignal(ISignal signal)
        {
            throw new NotImplementedException();
        }
    }
}
