﻿using System;
using System.IO;

namespace OscilloscopeEmulation
{
    public class Config
    {
        private const string Filename = "config.ini";

        public static string Get(string varname)
        {
            if (!File.Exists(Filename)) return null;
            string s;
            using (var file = new StreamReader(Filename))
            {
                string value = null;
                while ((s = file.ReadLine()) != null)
                {
                    string tempstring = "[" + varname + "]";

                    if (s.IndexOf(tempstring, StringComparison.Ordinal) != -1)
                    {
                        value = file.ReadLine();

                    }
                }
                return value;
            }
        }
    }
}
