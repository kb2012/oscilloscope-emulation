﻿using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Windows.Input;
using OscilloscopeEmulation.Signals;
using System.Globalization;

namespace OscilloscopeEmulation.SignalSettingsPanels 
{
    /// <summary>
    /// Interaction logic for InterferenceSettingsPanel.xaml
    /// </summary>
    public partial class InterferenceSettingsPanel : ISignalSettingsPanel
    {
        public InterferenceSettingsPanel()
        {
            InitializeComponent();
        }

        public ISignal GetSignal()
        {
            return new Interference(double.Parse(LowInterferenceLimitTextBox.Text, CultureInfo.InvariantCulture),
                double.Parse(HighInterferenceLimitTextBox.Text, CultureInfo.InvariantCulture));
        }

        private void ValidateInputText(TextCompositionEventArgs e, TextBox textBox)
        {
            var amountDots = textBox.Text.Count(x => x == '.');
            if (amountDots == 1 && e.Text == ".")
            {
                e.Handled = true;
            }
            else
            {
                var regex = new Regex(@"^[\-]{0,1}[0-9\.]*$");
                e.Handled = !regex.IsMatch(e.Text);
            }
        }

        private void HighInterferenceLimitTextBox_OnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            ValidateInputText(e, sender as TextBox);
        }

        private void LowInterferenceLimitTextBox_OnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            ValidateInputText(e, sender as TextBox);
        }
    }
}
