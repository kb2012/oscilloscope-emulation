﻿using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Windows.Input;
using OscilloscopeEmulation.Signals;

namespace OscilloscopeEmulation.SignalSettingsPanels
{
    /// <summary>
    /// Interaction logic for ConstantSignalSettingsPanel.xaml
    /// </summary>
    public partial class ConstantSignalSettingsPanel : ISignalSettingsPanel
    {
        public ConstantSignalSettingsPanel()
        {
            InitializeComponent();
        }

        public ISignal GetSignal()
        {
            var constantSignal = new ConstantSignal(double.Parse(AmplitudeTextBox.Text));
            return constantSignal;
        }

        private void AmplitudeTextBox_OnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var amountDots = AmplitudeTextBox.Text.Count(x => x == '.');
            if (amountDots == 1 && e.Text == ".")
            {
                e.Handled = true;
            }
            else
            {
                var regex = new Regex(@"^[\-]{0,1}[0-9\.]*$");
                e.Handled = !regex.IsMatch(e.Text);
            }
        }
    }
}
