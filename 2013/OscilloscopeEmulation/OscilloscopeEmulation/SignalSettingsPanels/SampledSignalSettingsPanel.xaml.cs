﻿using System;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using OscilloscopeEmulation.Signals;
using System.Globalization;

namespace OscilloscopeEmulation.SignalSettingsPanels
{
    /// <summary>
    /// Interaction logic for SampledSignalSettingsPanel.xaml
    /// </summary>
    public partial class SampledSignalSettingsPanel : ISignalSettingsPanel
    {
        public CompositeSignal CompositeChannelSignal;
        public DiscretizedSignal ResultingDiscretizedSignalSignal;
        public SampledSignalSettingsPanel(CompositeSignal compositeSignal)
        {
            InitializeComponent();
            CompositeChannelSignal = compositeSignal;
            SaveSampledSignalSettingsImageButton.Source =
                 new BitmapImage(new Uri("../Images/Buttons/SaveSignalChangesButton.jpg", UriKind.Relative));
        }

        public ISignal GetSignal()
        {
            return ResultingDiscretizedSignalSignal;
        }

        private void SaveSampledSignalSettingsImageButton_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            ResultingDiscretizedSignalSignal = new DiscretizedSignal(CompositeChannelSignal, int.Parse(SamplingRatesTextBox.Text, CultureInfo.InvariantCulture));
        }
    }
}
