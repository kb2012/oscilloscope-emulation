﻿using System;
using System.Drawing;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace OscilloscopeEmulation
{
    public class ImageButtonProvider
    {
        public static System.Windows.Controls.Image GetImageButtonByPath(string path)
        {
            var image = new System.Windows.Controls.Image();
            var bitmapImage = new BitmapImage();
            bitmapImage.BeginInit();
            bitmapImage.UriSource =
                new Uri(path, UriKind.RelativeOrAbsolute);
            bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
            bitmapImage.EndInit();
            image.Source = bitmapImage;
            image.Stretch = Stretch.Uniform;
            return image;
        }

        public static BitmapImage GetImageBitmapSource(string path)
        {
            var bitmapImage = new BitmapImage();
            bitmapImage.BeginInit();
            bitmapImage.UriSource =
                new Uri(path, UriKind.RelativeOrAbsolute);
            bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
            bitmapImage.EndInit();
            return bitmapImage;
        }
    }
}
