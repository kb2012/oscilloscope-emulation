﻿using System;
using System.Drawing;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using OscilloscopeEmulation.Signals;

namespace OscilloscopeEmulation.ChannelPanels
{
    /// <summary>
    /// Interaction logic for SignalItem.xaml
    /// </summary>
    public partial class SignalItem : UserControl
    {
        public ISignal Signal { get; set; }
        public ChannelConfigurationStackPanel ChannelConfigurationStackPanel;
        public CompositeSignal _channelCompositeSignal;
        public ChannelItem ChannelItem;
        private SignalConfigurator _signalConfigurator;
        public SignalItem(ISignal signal, ChannelConfigurationStackPanel channelConfigurationStackPanel, CompositeSignal channelCompositeSignal, ChannelItem channelItem)
        {
            InitializeComponent();
            Signal = signal;
            _channelCompositeSignal = channelCompositeSignal;
            ChannelItem = channelItem;
            SignalNameTextBlock.Text = signal.ToString();
            ChannelConfigurationStackPanel = channelConfigurationStackPanel;
            var bugFixImageSource = new BitmapImage(new Uri(@"../Images/Buttons/SignalItemBackground.png", UriKind.Relative));
            EditingSignalImageButton.Source = bugFixImageSource;
            EditingSignalImageButton.Source =
                 new BitmapImage(new Uri("../Images/Buttons/EditIcon.png", UriKind.Relative));

            SignalItemStackPanel.Background = new ImageBrush
            {
                ImageSource = bugFixImageSource
            };
        }

        private void UIElement_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                ChannelConfigurationStackPanel.ConfigurationCurrentSignalStackPanel.Children.Clear();
                if (_signalConfigurator == null)
                    _signalConfigurator = new SignalConfigurator(Signal, _channelCompositeSignal, ChannelItem, ChannelConfigurationStackPanel);
                ChannelConfigurationStackPanel.ConfigurationCurrentSignalStackPanel.Children.Add(_signalConfigurator);
            }
            catch
            {
                MessageBox.Show("Что-то пошло не так!");
            }
        }

        private void ActivatingSignalCheckBox_OnChecked(object sender, RoutedEventArgs e)
        {
            var children = ChannelConfigurationStackPanel.SignalItemsStackPanel.Children;
            foreach (var child in children)
            {
                if (child is SignalItem)
                {
                    if (!Equals(child as SignalItem))
                        (child as SignalItem).ActivatingAmplitudeModulationCheckBox.IsChecked = false;
                }
            }
        }
    }
}
