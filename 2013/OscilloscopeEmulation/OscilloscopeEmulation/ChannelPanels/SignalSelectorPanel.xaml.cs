﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using OscilloscopeEmulation.Signals;
using OscilloscopeEmulation.SignalSettingsPanels;

namespace OscilloscopeEmulation.ChannelPanels
{
    /// <summary>
    /// Interaction logic for SignalSelectorPanel.xaml
    /// </summary>
    public partial class SignalSelectorPanel : UserControl
    {
        private ISignalSettingsPanel _currentSignalSettingsPanel;
        private CompositeSignal _currentCompositeSignal;
        private ChannelItem _channelItem;
        public SignalSelectorPanel(CompositeSignal currentCompositeSignal, ChannelItem channelItem)
        {
            InitializeComponent();
            _currentCompositeSignal = currentCompositeSignal;
            _channelItem = channelItem;
            ComponentCompositeSignalListBox.Items.Add("Постоянный сигнал");
            ComponentCompositeSignalListBox.Items.Add("Дельта-функция Дирака");
            ComponentCompositeSignalListBox.Items.Add("Кодированный сигнал");
            ComponentCompositeSignalListBox.Items.Add("Гармонический сигнал");
            ComponentCompositeSignalListBox.Items.Add("Помехи");
            AddCurrentSignalToCompositeSignal.Source =
                new BitmapImage(new Uri("../Images/Buttons/AddSignalToChannelButton.jpg", UriKind.Relative));
        }

        private void ComponentCompositeSignalListBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_currentSignalSettingsPanel != null)
            {
                SignalSelectorStackPanel.Children.Remove(_currentSignalSettingsPanel as UserControl);
            }
            
            switch (e.AddedItems[0].ToString())
            {
                case "Постоянный сигнал": _currentSignalSettingsPanel = new ConstantSignalSettingsPanel();
                    break;
                case "Дельта-функция Дирака": _currentSignalSettingsPanel = new DiracDeltaFunctionSettingsPanel();
                    break;
                case "Кодированный сигнал": _currentSignalSettingsPanel = new EncodedSignalSettingsPanel();
                    break;
                case "Гармонический сигнал": _currentSignalSettingsPanel = new HarmonicSignalSettingsPanel();
                    break;
                case "Помехи": _currentSignalSettingsPanel = new InterferenceSettingsPanel();
                    break;
                //case "Sampled Signal": _currentSignalSettingsPanel = new SampledSignalSettingsPanel();
                //    break;
            }
            SignalSelectorStackPanel.Children.Add(_currentSignalSettingsPanel as UserControl);
        }

        private void AddCurrentSignalToCompositeSignal_OnClick(object sender, RoutedEventArgs e)
        {
            if (_currentSignalSettingsPanel != null)
            {
                ISignal signal;
                try
                {
                    signal = _currentSignalSettingsPanel.GetSignal();
                    _currentCompositeSignal.Add(signal);
                    _channelItem.RefreshSignalComponentVisualization();
                }
                catch (Exception)
                {
                    MessageBox.Show("Не указаны параметры сигнала!");
                }
                //_currentCompositeSignal.Add(_currentSignalSettingsPanel.GetSignal());
                //_channelItem.RefreshSignalComponentVisualization();
            }
        }
    }
}
