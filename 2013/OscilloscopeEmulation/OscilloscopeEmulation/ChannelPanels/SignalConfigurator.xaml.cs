﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms.VisualStyles;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using OscilloscopeEmulation.Signals;
using OscilloscopeEmulation.SignalSettingsPanels;

namespace OscilloscopeEmulation.ChannelPanels
{
    /// <summary>
    /// Interaction logic for SignalConfigurator.xaml
    /// </summary>
    public partial class SignalConfigurator : UserControl
    {
        public ISignal Signal { get; set; }
        public ISignalSettingsPanel SettingsPanel { get; set; }
        public CompositeSignal CompositeChannelSignal { get; set; }
        public ChannelItem ChannelItem { get; set; }
        public ChannelConfigurationStackPanel ChannelConfigurationStackPanel { get; set; }
        public SignalConfigurator(ISignal signal, CompositeSignal compositeChannelSignal, ChannelItem channelItem, ChannelConfigurationStackPanel channelConfigurationStackPanel)
        {
            InitializeComponent();
            Signal = signal;
            CompositeChannelSignal = compositeChannelSignal;
            ChannelItem = channelItem;
            ChannelConfigurationStackPanel = channelConfigurationStackPanel;
            SettingsPanel = signal.GetSignalSettingsPanel();
            SaveChangesButton.Source =
                 new BitmapImage(new Uri("../Images/Buttons/SaveSignalChangesButton.jpg", UriKind.Relative));
            RemoveSignalFromChannel.Source =
                new BitmapImage(new Uri("../Images/Buttons/RemoveSignalFromChannelButton.jpg", UriKind.Relative));

            var parent = VisualTreeHelper.GetParent(SettingsPanel as UserControl);
            var parentAsPanel = parent as Panel;
            if (parentAsPanel != null)
            {
                parentAsPanel.Children.Remove(SettingsPanel as UserControl);
            }

            SelectedSignalSettingsStackPanel.Children.Add(SettingsPanel as UserControl);
        }

        private void SaveChangesButton_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                Signal.CloneSignal(SettingsPanel.GetSignal());
            }
            catch (Exception)
            {
                MessageBox.Show("Не указаны параметры для изменения настроек сигнала!");
            }
            ChannelItem.RefreshSignalComponentVisualization();
            SelectedSignalSettingsStackPanel.Children.Clear();
            ChannelConfigurationStackPanel.ConfigurationCurrentSignalStackPanel.Children.Clear();
        }

        private void RemoveSignalFromChannel_OnClick(object sender, RoutedEventArgs e)
        {
            CompositeChannelSignal.Signals.Remove(Signal);
            ChannelItem.RefreshSignalComponentVisualization();
            SelectedSignalSettingsStackPanel.Children.Clear();
            ChannelConfigurationStackPanel.ConfigurationCurrentSignalStackPanel.Children.Clear();
        }
    }
}
