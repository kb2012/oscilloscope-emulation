﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace OscilloscopeEmulation.ChannelPanels
{
    /// <summary>
    /// Interaction logic for ChannelConfigurationStackPanel.xaml
    /// </summary>
    public partial class ChannelConfigurationStackPanel
    {
        public List<ChannelItem> ChannelItems = new List<ChannelItem>();
        private readonly List<Color> _colors = new List<Color>() { Colors.Blue, Colors.Green, Colors.Orange, Colors.Purple, Colors.Yellow, Colors.OrangeRed, Colors.DimGray };
        public ChannelConfigurationStackPanel()
        {
            InitializeComponent();
            AddingNewChannelImageButton.Source =
                new BitmapImage(new Uri("../Images/Buttons/AddChannelButton.jpg", UriKind.Relative));
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            var newChannelItem = new ChannelItem(string.Format("Channel-{0}", ChannelItems.Count), this, _colors[ChannelItems.Count % _colors.Count]);
            ChannelItems.Add(newChannelItem);
            ChannelStackPanel.Children.Add(newChannelItem);
            for (var i = 0; i < ChannelItems.Count; i++)
            {
                var channelItem = ChannelItems[i];
                var selectedItem = channelItem.InputChannelsComboBox.Text;
                channelItem.InputChannelsComboBox.Items.Clear();
                for (var j = 0; j < ChannelItems.Count; j++)
                {
                    if(i == j)
                        continue;

                    var item = ChannelItems[j];
                    var comboBoxItem = new ComboBoxItem { Content = item.ChannelItemName };
                    channelItem.InputChannelsComboBox.Items.Add(comboBoxItem);
                } 
                channelItem.InputChannelsComboBox.Text = selectedItem;
            }
        }
    }
}
