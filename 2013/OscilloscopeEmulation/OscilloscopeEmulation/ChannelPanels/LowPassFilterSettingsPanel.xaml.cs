﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using OscilloscopeEmulation.Signals;
using System.Globalization;

namespace OscilloscopeEmulation.ChannelPanels
{
    /// <summary>
    /// Interaction logic for LowPassFilterSettingsPanel.xaml
    /// </summary>
    public partial class LowPassFilterSettingsPanel
    {
        private CompositeSignal _compositeChannelSignal;
        public LowFilteredSignal ResultingLowFilteredSignal;

        public LowPassFilterSettingsPanel(CompositeSignal compositeChannelSignal)
        {
            InitializeComponent();
            _compositeChannelSignal = compositeChannelSignal;
            SaveLowFilterPassSettingsImageButton.Source =
                new BitmapImage(new Uri("../Images/Buttons/SaveSignalChangesButton.jpg", UriKind.Relative));
        }

        private void SaveLowFilterPassSettingsImageButton_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            ResultingLowFilteredSignal = new LowFilteredSignal(_compositeChannelSignal, int.Parse(FilterLengthTextBox.Text, CultureInfo.InvariantCulture),
                double.Parse(SamplingFrequencyTextBox.Text, CultureInfo.InvariantCulture), double.Parse(FrequencyBandwidthTextBox.Text, CultureInfo.InvariantCulture), double.Parse(FrequencyBandAttenuationTextBox.Text, CultureInfo.InvariantCulture));
            if (!string.IsNullOrEmpty(AmplificationFactorTextBox.Text))
            {
                double amplificationFactor;
                if (double.TryParse(AmplificationFactorTextBox.Text, NumberStyles.None, CultureInfo.InvariantCulture, out amplificationFactor))
                    LowFilteredSignal.Multiplier = amplificationFactor;
            }
        }
    }
}
