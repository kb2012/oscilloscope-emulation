﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Threading;
using Image = System.Windows.Controls.Image;

namespace OscilloscopeEmulation
{
    public class Animation
    {
        private readonly DispatcherTimer _timer = new DispatcherTimer(DispatcherPriority.Render);
        private int _currentFrame;
        readonly Image _imageControl;
        readonly List<BitmapImage> _bitMapImages = new List<BitmapImage>();

        public Window GeneralWindow { get; set; }
        public Window WindowWithLogo { get; set; }

        public Animation(Image imageControl, Window windowWithLogo, Window generalWindow)
        {
            GeneralWindow = generalWindow;
            WindowWithLogo = windowWithLogo;
            generalWindow.Visibility = Visibility.Hidden;

            _currentFrame = 0;
            _imageControl = imageControl;
            for (var i = 90; i < 281; i++)
            {
                BitmapImage bitmap;
                if (i < 100)
                    bitmap = new BitmapImage(new Uri(string.Format("Images/Animation/LogoAnimation_000{0}.jpg", i), UriKind.Relative));
                else
                    bitmap = new BitmapImage(new Uri(string.Format("Images/Animation/LogoAnimation_00{0}.jpg", i), UriKind.Relative));

                _bitMapImages.Add(bitmap);
            }
        }

        public void Animate()
        {
            _timer.Tick += OnImageRefreshEvent;
            _timer.Interval = new TimeSpan(0, 0, 0, 0, 30);
            _timer.IsEnabled = true;
            _timer.Start();
        }

        public void Stop()
        {
            _timer.Stop();
            WindowWithLogo.Visibility = Visibility.Hidden;
            GeneralWindow.Visibility = Visibility.Visible;
        }

        public void OnImageRefreshEvent(object sender, EventArgs e)
        {
            if (_currentFrame < _bitMapImages.Count)
            {
                _imageControl.Source = _bitMapImages[_currentFrame];
                _currentFrame++;
            }
            else
            {
                _timer.Stop();
                Thread.Sleep(1500);
                WindowWithLogo.Visibility = Visibility.Hidden;
                GeneralWindow.Visibility = Visibility.Visible;
            }
        }
    }
}
