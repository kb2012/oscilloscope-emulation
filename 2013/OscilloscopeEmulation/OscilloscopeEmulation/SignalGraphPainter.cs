﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using OscilloscopeEmulation.Signals;
using ZedGraph;

namespace OscilloscopeEmulation
{
    public class SignalGraphPainter //Рисует основные графики
    {
        public static int TotalEntries = 10000; //Количество точек на графике
        public static int TotalMSec = 10;

        public double MinX = 0;
        public double MaxX = TotalEntries;
        public double MinY = -2;
        public double MaxY = 2;

        public int KiloHz = 1000; //Константа, для того, чтобы было понятно, что умножаем на 100 из-за kHz
        public int MsecPerValue = 10; //Каждые 10 мсек на графике точка
        public int ScaleYFactor = 20; //Растягиваем по игреку для удобства
        public int StartZoom = 5; //Стартовое увеличение

        private ZedGraphControl _zedGraph;
        public bool IsFixingTriangleIssue = false;


        public SignalGraphPainter(bool isFixingTriangleIssue)
        {
            IsFixingTriangleIssue = isFixingTriangleIssue;
        }

        public void InitializeGraph(ZedGraphControl zedGraph) //Переделать только под signal
        {
            _zedGraph = zedGraph;

            // Получим панель для рисования
            GraphPane pane = zedGraph.GraphPane;

            pane.Title.Text = "Область сигналов";
            pane.YAxis.Title.Text = "Амплитуда, В";
            
            zedGraph.IsShowHScrollBar = true;
            zedGraph.IsShowVScrollBar = true;
            zedGraph.IsAutoScrollRange = true;

            pane.XAxis.IsVisible = false; //X пусть будет невидим на этом окне, т.к. там значения надо пересчитывать(см. функцию показа значений)


            pane.XAxis.Scale.Min = MinX;
            pane.XAxis.Scale.Max = MaxX;

            //todo fix auto to constants if it will works better - не, вроде норм
            //Автоматически подгоним маштаб по Y
            pane.YAxis.Scale.MinAuto = true;
            pane.YAxis.Scale.MaxAuto = true;

            // Очистим список кривых на тот случай, если до этого сигналы уже были нарисованы
            pane.CurveList.Clear();

            zedGraph.ZoomEvent += zedGraph_ZoomEvent;

            // Включим показ всплывающих подсказок при наведении курсора на график
            zedGraph.IsShowPointValues = true;
            zedGraph.PointValueEvent += zedGraph_PointValueEvent_Recalc;
        }

        public void DrawSignals(IList<ISignal> signals, IList<Color> colors)
        {
            GraphPane pane = _zedGraph.GraphPane;
            MinY = -25;
            MaxY = 25;
            MinX = 0;
            MaxX = 5000;

            pane.YAxis.Scale.Min = MinY / StartZoom;
            pane.YAxis.Scale.Max = MaxY / StartZoom;
            pane.XAxis.Scale.Max = MaxX / StartZoom;


            for (var i = 0; i < signals.Count(); i++)
            {
                var points = signals[i].GetValuesList(TotalMSec, TotalEntries);
                var f1List = new PointPairList();
                for (var x = 0; x < TotalEntries; x++)
                {
                    f1List.Add(x, points[x]);
                }
                pane.AddCurve(string.Format("Channel-{0}", i), f1List, colors[i], SymbolType.None);
            }
        }

        public void DrawSignal(ISignal signal, Color color, bool axisChange = true)
        {
            if (_zedGraph == null) throw new InvalidDataException("Граф не проинициализирован");
            DrawLine(_zedGraph, signal.GetValuesList(TotalMSec, TotalEntries), signal.GetName(), color);

            if (axisChange)
            {
                var zedGraph = _zedGraph;
                // Получим панель для рисования
                GraphPane pane = zedGraph.GraphPane;

                // Вызываем метод AxisChange (), чтобы обновить данные об осях. 
                // В противном случае на рисунке будет показана только часть графика, 
                // которая умещается в интервалы по осям, установленные по умолчанию
                zedGraph.AxisChange();

                // Обновляем график
                zedGraph.Invalidate();

                //Уменьшаем маштаб по Y для удобства (чтобы синусойда была не на всю высоту экрана)
                MinY = pane.YAxis.Scale.Min * ScaleYFactor;
                MaxY = pane.YAxis.Scale.Max * ScaleYFactor;

                pane.YAxis.Scale.Min = MinY / StartZoom;
                pane.YAxis.Scale.Max = MaxY / StartZoom;

                pane.XAxis.Scale.Max = MaxX / StartZoom;

                //Сдвинем по центру, например чтобы удобнее на дельта-функцию смотреть было
                var width = pane.XAxis.Scale.Max - pane.XAxis.Scale.Min;
                pane.XAxis.Scale.Max = pane.XAxis.Scale.Max + TotalEntries / 2.0 - width / 2;
                pane.XAxis.Scale.Min = pane.XAxis.Scale.Min + TotalEntries / 2.0 - width / 2;

                pane.AxisChange();
            }
        }

        public void DrawLine(ZedGraphControl zedGraph, IList<double> points, string name, Color color)
        {
            // Получим панель для рисования
            GraphPane pane = zedGraph.GraphPane;

            // Создадим список точек для кривой f1(x)
            PointPairList f1List = new PointPairList();

            //инициализируем класс с фиксом "треугольничков" и определяем, исправлять баг или же нет
            var triangleFix = new TriangleFixClass(IsFixingTriangleIssue);
            // Заполним массив точек для кривой f1(x)
            for (int x = 0; x < TotalEntries; x ++)
            {
                triangleFix.FixTriangleIssueForSignal(points, x, ref f1List);  
            }

            // !!!
            // Создадим кривую с названием *name*, 
            // которая будет рисоваться *color* цветом,
            // Опорные точки выделяться не будут (SymbolType.None)
            LineItem f1Curve = pane.AddCurve(name, f1List, color, SymbolType.None);

            zedGraph.Invalidate();

        }

        //Событие для установки мин и макс лимита зума
        void zedGraph_ZoomEvent(ZedGraphControl sender, ZoomState oldState, ZoomState newState)
        {
            GraphPane pane = sender.GraphPane;

            // Для простоты примера будем ограничивать масштабирование 
            // только в сторону уменьшения размера графика

            // Проверим интервал для каждой оси и 
            // при необходимости скорректируем его

            if (pane.XAxis.Scale.Min < 0)
            {
                pane.XAxis.Scale.Min = 0;
            }

            if (pane.XAxis.Scale.Max > TotalEntries)
            {
                pane.XAxis.Scale.Max = TotalEntries;
            }

            if (pane.YAxis.Scale.Min < MinY)
            {
                pane.YAxis.Scale.Min = MinY;
            }

            if (pane.YAxis.Scale.Max > MaxY)
            {
                pane.YAxis.Scale.Max = MaxY;
            }

        }

        /// <summary>
        /// Обработчик события PointValueEvent.
        /// Должен вернуть строку, которая будет показана во всплывающей подсказке
        /// </summary>
        /// <param name="sender">Отправитель сообщения</param>
        /// <param name="pane">Панель для рисования</param>
        /// <param name="curve">Кривая, около которой находится курсор</param>
        /// <param name="iPt">Номер точки в кривой</param>
        /// <returns>Нужно вернуть отображаемую строку</returns>
        string zedGraph_PointValueEvent(ZedGraphControl sender,
            GraphPane pane,
            CurveItem curve,
            int iPt)
        {
            // Получим точку, около которой находимся
            PointPair point = curve[iPt];

            // Сформируем строку
            string result = string.Format("X: {0:F3}\nY: {1:F3}", point.X, point.Y);
            return result;
        }

        string zedGraph_PointValueEvent_Recalc(ZedGraphControl sender,
            GraphPane pane,
            CurveItem curve,
            int iPt)
        {
            // Получим точку, около которой находимся
            PointPair point = curve[iPt];

            // Сформируем строку
            string result = string.Format("{0:F2} мсек\n{1:F2} В", point.X*TotalMSec/TotalEntries, point.Y);
            return result;
        }

    }
}
