﻿using System;
using System.Collections.Generic;
using System.Linq;
using ZedGraph;

namespace OscilloscopeEmulation
{
    public class TriangleFixClass
    {
        private bool IsEnabled { get; set; }

        public TriangleFixClass(bool isEnabled)
        {
            IsEnabled = isEnabled;
        }
        public void FixTriangleIssueForSignal(IList<double> points, int x, ref PointPairList f1List)
        {
            if ((f1List.Count >= 3) && (Math.Abs(points[x - 2].CompareTo(points[x - 1])) > 10 * Math.Abs(points[x - 2].CompareTo(points[x]))) && IsEnabled)
            {
                f1List.Remove(f1List.Last());
                f1List.Add(x - 1, points[x - 2] + (points[x] - points[x - 2]));
                f1List.Add(x - 1, points[x - 1]);
                f1List.Add(x - 1, points[x - 2] + (points[x] - points[x - 2]));
                f1List.Add(x, points[x]);
            }
            else f1List.Add(x, points[x]);
        }
        public void FixTriangleIssueForSpectrum(IList<double> points, int x, ref PointPairList f1List)
        {
            if (f1List.Count >= 2 && IsEnabled)
            {
                if ((points[x] > points[x - 1]))
                {
                    f1List.Add(x, points[x - 1]);
                    f1List.Add(x, points[x]);
                }
                if (!(points[x] < points[x - 1])) return;
                f1List.Add(x - 1, points[x]);
                f1List.Add(x, points[x]);
            }
            else
                f1List.Add(x, points[x]);
        }
    }
}
