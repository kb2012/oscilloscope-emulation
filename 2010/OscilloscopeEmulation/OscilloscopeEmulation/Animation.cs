﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace OscilloscopeEmulation
{
    public class Animation
    {
        private readonly DispatcherTimer _timer = new DispatcherTimer(DispatcherPriority.Render);
        readonly List<byte[]> _bitMapImages = new List<byte[]>();
        private int _currentFrame;
        readonly Image _imageControl;

        public Window GeneralWindow { get; set; }
        public Window WindowWithLogo { get; set; }

        public Animation(String pathToDirectory, Image imageControl, Window windowWithLogo, Window generalWindow)
        {
            GeneralWindow = generalWindow;
            WindowWithLogo = windowWithLogo;
            generalWindow.Visibility = Visibility.Hidden;

            _currentFrame = 0;
            _imageControl = imageControl;
            var pathes = Directory.GetFiles(pathToDirectory, "*.*", SearchOption.AllDirectories).ToArray();
            foreach (var path in pathes)
                _bitMapImages.Add(File.ReadAllBytes(path));
        }

        public void Animate()
        {
            _timer.Tick += OnImageRefreshEvent;
            _timer.Interval = new TimeSpan(0, 0, 0, 0, 30);
            _timer.IsEnabled = true;
            _timer.Start();
        }

        public void OnImageRefreshEvent(object sender, EventArgs e)
        {
            if (_currentFrame < _bitMapImages.Count)
            {
                _imageControl.Source = GetCurrPicture(_currentFrame);
                _currentFrame++;
            }
            else
            {
                _timer.Stop();
                Thread.Sleep(1500);
                WindowWithLogo.Visibility = Visibility.Hidden;
                GeneralWindow.Visibility = Visibility.Visible;
            }
        }

        private BitmapImage GetCurrPicture(int index)
        {
            var bitmapImage = new BitmapImage();
            using (Stream stream = new MemoryStream(_bitMapImages[index]))
            {
                bitmapImage.BeginInit();
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.StreamSource = stream;
                bitmapImage.EndInit();
                bitmapImage.Freeze();
                return bitmapImage;
            }
        }
    }
}
