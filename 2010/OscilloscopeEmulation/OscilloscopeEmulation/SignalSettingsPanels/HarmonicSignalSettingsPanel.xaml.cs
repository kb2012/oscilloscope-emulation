﻿using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Windows.Input;
using OscilloscopeEmulation.Signals;
using System.Globalization;

namespace OscilloscopeEmulation.SignalSettingsPanels
{
    /// <summary>
    /// Interaction logic for HarmonicSignalSettingsPanel.xaml
    /// </summary>
    public partial class HarmonicSignalSettingsPanel : ISignalSettingsPanel
    {
        public HarmonicSignalSettingsPanel()
        {
            InitializeComponent();
        }

        public ISignal GetSignal()
        {
            var harmonicSignal = new HarmonicSignal(double.Parse(AmplitudeTextBox.Text, CultureInfo.InvariantCulture),
                double.Parse(FrequencyTextBox.Text, CultureInfo.InvariantCulture), double.Parse(StartPhaseTextBox.Text, CultureInfo.InvariantCulture));
            return harmonicSignal;
        }

        private void ValidateInputText(TextCompositionEventArgs e, TextBox textBox)
        {
            var amountDots = textBox.Text.Count(x => x == '.');
            if (amountDots == 1 && e.Text == ".")
            {
                e.Handled = true;
            }
            else
            {
                var regex = new Regex(@"^[\-]{0,1}[0-9\.]*$");
                e.Handled = !regex.IsMatch(e.Text);
            }
        }

        private void StartPhaseTextBox_OnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            ValidateInputText(e, sender as TextBox);
        }

        private void FrequencyTextBox_OnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            ValidateInputText(e, sender as TextBox);
        }

        private void AmplitudeTextBox_OnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            ValidateInputText(e, sender as TextBox);
        }
    }
}
