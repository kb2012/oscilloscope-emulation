﻿
using OscilloscopeEmulation.Signals;

namespace OscilloscopeEmulation.SignalSettingsPanels
{
    /// <summary>
    /// Interaction logic for DiracDeltaFunctionSettingsPanel.xaml
    /// </summary>
    public partial class DiracDeltaFunctionSettingsPanel : ISignalSettingsPanel
    {
        public DiracDeltaFunctionSettingsPanel()
        {
            InitializeComponent();
        }

        public ISignal GetSignal()
        {
            return new DiracDeltaFunction();
        }
    }
}
