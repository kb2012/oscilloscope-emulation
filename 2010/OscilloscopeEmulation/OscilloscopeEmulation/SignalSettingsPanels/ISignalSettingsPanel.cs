﻿using System;
using OscilloscopeEmulation.Signals;

namespace OscilloscopeEmulation.SignalSettingsPanels
{
    public interface ISignalSettingsPanel
    {
        ISignal GetSignal();
    }
}
