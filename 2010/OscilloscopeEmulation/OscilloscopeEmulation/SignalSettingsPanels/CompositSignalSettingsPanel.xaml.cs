﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using OscilloscopeEmulation.Signals;

namespace OscilloscopeEmulation.SignalSettingsPanels
{
    /// <summary>
    /// Логика взаимодействия для CompositSignalSettingsPanel.xaml
    /// </summary>
    public partial class CompositSignalSettingsPanel : ISignalSettingsPanel
    {
        public List<ISignal> AllSignals = new List<ISignal>();
        public Action<ISignal> DrawSignalAction { get; set; }

        public CompositSignalSettingsPanel()
        {
            InitializeComponent();
        }

        public ISignal GetSignal()
        {
            return null;
        }

        private void FrequencyDiscret_OnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
