﻿using System;
using System.Text.RegularExpressions;
using System.Windows.Input;
using OscilloscopeEmulation.Signals;

namespace OscilloscopeEmulation.SignalSettingsPanels
{
    /// <summary>
    /// Логика взаимодействия для EncodedSignalSettingsPanel.xaml
    /// </summary>
    public partial class EncodedSignalSettingsPanel : ISignalSettingsPanel
    {
        public EncodedSignalSettingsPanel()
        {
            InitializeComponent();
        }


        public ISignal GetSignal()
        {
            if (SignalCodeTextBox.Text.Length == 0)
                throw new ArgumentException();
            return new EncodedSignal(SignalCodeTextBox.Text);
        }

        private void SignalCodeTextBox_OnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var regex = new Regex("[^0-1]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
