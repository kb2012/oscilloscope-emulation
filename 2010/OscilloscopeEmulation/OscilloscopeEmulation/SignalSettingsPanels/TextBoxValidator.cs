﻿using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Windows.Input;

namespace OscilloscopeEmulation.SignalSettingsPanels
{
    public class TextBoxValidator
    {
        private static void ValidateInputText(TextCompositionEventArgs e, TextBox textBox)
        {
            var amountDots = textBox.Text.Count(x => x == '.');
            if (amountDots == 1 && e.Text == ".")
            {
                e.Handled = true;
            }
            else
            {
                var regex = new Regex(@"^[\-]{0,1}[0-9\.]*$");
                e.Handled = !regex.IsMatch(e.Text);
            }
        }
    }
}
