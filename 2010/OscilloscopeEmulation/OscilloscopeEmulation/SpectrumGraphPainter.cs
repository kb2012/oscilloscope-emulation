﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using OscilloscopeEmulation.Signals;
using ZedGraph;

namespace OscilloscopeEmulation
{
    public class SpectrumGraphPainter //Для спектра отдельный граф накопипастили к сожалению
    {
        public static int TotalEntries = 4096; //Количество точек на графике
        public static int TotalMSec = 10;

        private ZedGraphControl _zedGraph;
        public bool IsFixingTriangleIssueRendering;

        public SpectrumGraphPainter(bool isFixingTriangleIssueRendering)
        {
            IsFixingTriangleIssueRendering = isFixingTriangleIssueRendering;
        }

        public void InitializeGraph(ZedGraphControl zedGraph) //Переделать только под signal
        {
            _zedGraph = zedGraph;

            // Получим панель для рисования
            GraphPane pane = zedGraph.GraphPane;

            pane.Title.Text = "Область спектров";
            pane.YAxis.Title.Text = "Амплитуда, 10^-3 В";
            zedGraph.IsShowHScrollBar = true;
            zedGraph.IsShowVScrollBar = true;
            zedGraph.IsAutoScrollRange = true;

            pane.XAxis.IsVisible = false; //X пусть будет невидим на этом окне, т.к. там значения надо пересчитывать(см. функцию показа значений)

            //todo fix auto to constants if it will works better
            pane.XAxis.Scale.MinAuto = true;
            pane.XAxis.Scale.MaxAuto = true;
            pane.YAxis.Scale.MinAuto = true;
            pane.YAxis.Scale.MaxAuto = true;

            //pane.Margin.Right = 0;

            // Очистим список кривых на тот случай, если до этого сигналы уже были нарисованы
            pane.CurveList.Clear();

            // Включим показ всплывающих подсказок при наведении курсора на график
            zedGraph.IsShowPointValues = true;
            zedGraph.PointValueEvent += zedGraph_PointValueEvent;

            //Test Zone, todo remove
            //DrawLine(zedGraph, new Spectrum().GetSpectrumFromList(GetSinGraph(1, 1)), "Спектр Сложного сигнала", Color.Red);
        }

        public IList<double> GetSinGraph(double amplitude, double frequency, double startPhase = 0) //частота в кГц, старт. фаза по идее всегда 0
        {
            //Для теста 2 частоты и одна константа - cos wt, cos 2 wt и постоянный уровент 0.5В
            //todo выяснить почему именно такие высоты у спектров, у константного все норм и правильно, у других - хз
            var list = new List<double>();
            for (var i = 0; i < TotalEntries; i++)
            {
                //A*cos(2*Pi*f*t+phase0), t = i/TotalMSec
                //Всего 10 мсек, когда i = 1024 t = 10. В остальном кГц компенсируют миллисекунды (Умножили и поделили на 1000)
                var scaleFactor = (double)TotalMSec / TotalEntries;
                var wt = 2*Math.PI*frequency*i *scaleFactor; //A*Cos(2*pi*f*t + phase0) = A*cos(w*t + phase0)
                list.Add(amplitude * Math.Cos(wt + startPhase) + amplitude * Math.Cos(wt*2 + startPhase) + 0.5);
            }
            return list;
        }

        public void DrawSignal(ISignal signal, Color color)
        {
            if (_zedGraph == null) throw new InvalidDataException("Граф не проинициализирован");
            DrawLine(_zedGraph, new Spectrum().GetSpectrumFromList(signal.GetValuesList(TotalMSec, TotalEntries)), signal.GetName()+", Спектр", color);
        }

        public void DrawLine(ZedGraphControl zedGraph, IList<double> points, string name, Color color)
        {
            // Получим панель для рисования
            GraphPane pane = zedGraph.GraphPane;

            // Создадим список точек для кривой f1(x)
            PointPairList f1List = new PointPairList();

            //инициализируем класс с фиксом "треугольничков" и определяем, исправлять баг или же нет
            var triangleFix = new TriangleFixClass(IsFixingTriangleIssueRendering);
            // Заполним массив точек для кривой f1(x)
            for (int x = 0; x < TotalEntries; x++) //todo fix TotalEntries to constant
            {
                triangleFix.FixTriangleIssueForSpectrum(points, x, ref f1List);  
            }

            // !!!
            // Создадим кривую с названием *name*, 
            // которая будет рисоваться *color* цветом,
            // Опорные точки выделяться не будут (SymbolType.None)
            LineItem f1Curve = pane.AddCurve(name, f1List, color, SymbolType.None);

            // Вызываем метод AxisChange (), чтобы обновить данные об осях. 
            // В противном случае на рисунке будет показана только часть графика, 
            // которая умещается в интервалы по осям, установленные по умолчанию
            zedGraph.AxisChange();

            // Обновляем график
            zedGraph.Invalidate();

        }


        /// <summary>
        /// Обработчик события PointValueEvent.
        /// Должен вернуть строку, которая будет показана во всплывающей подсказке
        /// </summary>
        /// <param name="sender">Отправитель сообщения</param>
        /// <param name="pane">Панель для рисования</param>
        /// <param name="curve">Кривая, около которой находится курсор</param>
        /// <param name="iPt">Номер точки в кривой</param>
        /// <returns>Нужно вернуть отображаемую строку</returns>
        string zedGraph_PointValueEvent(ZedGraphControl sender, //todo нормальный вывод и пересчет значений для Спектров
            GraphPane pane,
            CurveItem curve,
            int iPt)
        {
            // Получим точку, около которой находимся
            PointPair point = curve[iPt];

            // Сформируем строку
            string result = String.Format("{0:F3} кГц\n{1:F3} В", GetFreq(point.X,TotalEntries,TotalEntries), point.Y/TotalEntries);
            return result;
        }

        public double GetFreq(double k, int n, double samplingFrequency)
        {
            samplingFrequency /= TotalMSec;
            return ((k - n / 2.0) * samplingFrequency / n);
        }
    }
}
