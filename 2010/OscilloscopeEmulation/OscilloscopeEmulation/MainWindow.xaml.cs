﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using OscilloscopeEmulation.ChannelPanels;
using OscilloscopeEmulation.Signals;
using ZedGraph;
using Application = System.Windows.Application;
using Color = System.Drawing.Color;
using OscilloscopeEmulation.SignalSettingsPanels;
using System.Globalization;

namespace OscilloscopeEmulation
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public ZedGraphControl SignalsGraph;
        public ZedGraphControl SpectrumGraph;
        private SignalGraphPainter _signalGraphPainter;
        private SpectrumGraphPainter _spectrumGraphPainter;
        private ISignal _currentSignal;
        private ISignalSettingsPanel _currentSignalSettingsPanel;
        private ChannelConfigurationStackPanel _channelConfigurationStackPanel;
        private readonly DispatcherTimer _timer = new DispatcherTimer(DispatcherPriority.Render);
        //private readonly LogoAnimationWindow _logoAnimationWindow = new LogoAnimationWindow();

        public MainWindow()
        {
            InitializeComponent();

            /* Settings */

            string value;
            value = Config.Get("Delta-Function-Height");
            if (value != null) DiracDeltaFunction.Height = double.Parse(value, CultureInfo.InvariantCulture);

            value = Config.Get("Encoded-Signal-Height");
            if (value != null) EncodedSignal.Height = double.Parse(value, CultureInfo.InvariantCulture);

            value = Config.Get("Encoded-Signal-Zero");
            if (value != null) EncodedSignal.ZeroLevel = double.Parse(value, CultureInfo.InvariantCulture);

            value = Config.Get("Encoded-Signal-Period");
            if (value != null) EncodedSignal.Period = double.Parse(value, CultureInfo.InvariantCulture);

            value = Config.Get("Min-Value-For-1");
            if (value != null) EncodedSignal.MinLevelFor1 = double.Parse(value, CultureInfo.InvariantCulture);

            value = Config.Get("Max-Value-For-0");
            if (value != null) EncodedSignal.MaxLevelFor0 = double.Parse(value, CultureInfo.InvariantCulture);

            value = Config.Get("Signal-Graph-Entries");
            if (value != null) SignalGraphPainter.TotalEntries = int.Parse(value, CultureInfo.InvariantCulture);

            value = Config.Get("Spectrum-Graph-Entries");
            if (value != null) SpectrumGraphPainter.TotalEntries = int.Parse(value, CultureInfo.InvariantCulture);


            /* End settings */

            RenderSignalsButton.Source = ImageButtonProvider.GetImageBitmapSource("../../ImageResource/Buttons/RenderSignalsButton.jpg");

            OscilloscopeEmulationWindow.Background = new ImageBrush
            {
                ImageSource = GetBitmapImageSource(@"../../ImageResource/Background.jpg"),
                TileMode = TileMode.Tile,
                Viewport = new Rect(0, 0, 1, 1)
            };
            ActivateProbabilityErrorCalculationImageButton.Source =
                ImageButtonProvider.GetImageBitmapSource("../../ImageResource/Buttons/ChangeModeButton.jpg");
            _timer.Tick += VisualizeSignalAndSpectrum;
            _timer.Interval = new TimeSpan(0, 0, 0, 0, 300);

            //_logoAnimationWindow.ShowLogo(OscilloscopeEmulationWindow);
            //_logoAnimationWindow.Show();
            //Closing += OnWindowClosing;

            _channelConfigurationStackPanel = new ChannelConfigurationStackPanel();
            GeneralChannelConfigurationStackPanel.Children.Add(_channelConfigurationStackPanel);
        }

        private BitmapImage GetBitmapImageSource(string path)
        {
            var bitmapImage = new BitmapImage();
            using (Stream stream = new MemoryStream(File.ReadAllBytes(path)))
            {
                bitmapImage.BeginInit();
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.StreamSource = stream;
                bitmapImage.EndInit();
                bitmapImage.Freeze();
                return bitmapImage;
            }
        }

        public void OnWindowClosing(object sender, CancelEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void RenderSignalsButton_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            VisualizeSignalAndSpectrum(null, null);
        }

        private void VisualizeSignalAndSpectrum(object sender, EventArgs e)
        {
            SignalsGraph = SignalsGraphControl;
            _signalGraphPainter = new SignalGraphPainter(FixingTriangleIssueRenderingCheckBox.IsChecked.Value);
            _signalGraphPainter.InitializeGraph(SignalsGraphControl);
            SpectrumGraph = SpectrumGraphControl;
            _spectrumGraphPainter = new SpectrumGraphPainter(FixingTriangleIssueRenderingCheckBox.IsChecked.Value);
            _spectrumGraphPainter.InitializeGraph(SpectrumGraph);

            var channelItems = _channelConfigurationStackPanel.ChannelItems;
            var signals = new List<ISignal>();
            var colors = new List<Color>();
            foreach (var channelItem in channelItems)
            {
                if (channelItem.SampledSignalSettingsPanel.ResultingDiscretizedSignalSignal == null)
                    channelItem.EnableSamplingCheckBox.IsChecked = false;
                if (channelItem.LowPassFilterSettingsPanel.ResultingLowFilteredSignal == null)
                    channelItem.EnableLowPassFilterCheckBox.IsChecked = false;

                if (!channelItem.ActivatingChannelCheckBox.IsChecked.Value)
                    continue;
                var selectedColor = channelItem.ChannelColorPicker.SelectedColor;
                var currentColor = Color.FromArgb(selectedColor.R, selectedColor.G, selectedColor.B);


                ISignal carrierSignal = null;
                var children = _channelConfigurationStackPanel.SignalItemsStackPanel.Children;
                var currentSignals = new List<ISignal>();
                foreach (var child in children)
                {
                    if (child is SignalItem)
                    {
                        var currentSignalItem = (child as SignalItem);

                        if ((currentSignalItem).ActivatingAmplitudeModulationCheckBox.IsChecked.Value)
                            carrierSignal = (currentSignalItem).Signal;
                        else
                            currentSignals.Add(currentSignalItem.Signal);
                    }
                }
                if (carrierSignal != null)
                {
                    var compositeSignal =
                        new CompositeSignal(channelItem.CurrentCompositeSignal.ChannelNumber);
                    compositeSignal.Signals.AddRange(currentSignals.ToArray());
                    var amplitudeModulation = new AmplitudeModulatedSignal(compositeSignal, carrierSignal);
                    signals.Add(amplitudeModulation);
                    colors.Add(currentColor);
                    _spectrumGraphPainter.DrawSignal(amplitudeModulation, currentColor);
                }
                else
                {
                    signals.Add(channelItem.GetResultingSignal());
                    colors.Add(currentColor);
                    if (channelItem.ShowSpectrumCheckBox.IsChecked.Value)
                        _spectrumGraphPainter.DrawSignal(channelItem.CurrentCompositeSignal, currentColor);
                }

                //RenderSignal(channelItem.CurrentCompositeSignal, currentColor, spectrumGraphPainter);
            }
            _signalGraphPainter.DrawSignals(signals, colors);

            //if (_currentSignalSettingsPanel != null)
            //{
            //    RenderSignal(_currentSignalSettingsPanel.GetSignal());
            //}

            //OldSinTestGraph();
            //DeltaFunctionTestGraph();
            //TestAm();
            //EncodedSignalTestGraph();
            //DiscretizedTestGraph();
        }

        private void TotalMsecTextBox_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            var totalMsec = int.Parse(TotalMsecTextBox.Text, CultureInfo.InvariantCulture);
            if (_signalGraphPainter != null && _spectrumGraphPainter != null)
            {
                SignalGraphPainter.TotalMSec = totalMsec;
                SpectrumGraphPainter.TotalMSec = totalMsec;
            }
        }

        private void StopingRefreshingVisualizationCheckBox_OnChecked(object sender, RoutedEventArgs e)
        {
            _timer.Start();
        }

        private void StopingRefreshingVisualizationCheckBox_OnUnchecked(object sender, RoutedEventArgs e)
        {
            _timer.Stop();
        }

        private void ShowAllSpectrumsCheckBox_OnChecked(object sender, RoutedEventArgs e)
        {
            var channelItems = _channelConfigurationStackPanel.ChannelItems;
            foreach (var currentChannelItem in channelItems)
            {
                currentChannelItem.ShowSpectrumCheckBox.IsChecked = true;
            }
        }

        private void ShowAllSpectrumsCheckBox_OnUnchecked(object sender, RoutedEventArgs e)
        {
            var channelItems = _channelConfigurationStackPanel.ChannelItems;
            foreach (var currentChannelItem in channelItems)
            {
                currentChannelItem.ShowSpectrumCheckBox.IsChecked = false;
            }
        }

        private void TotalMsecTextBox_OnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private EncodedSignalSettingsPanel _encodedSignalSettingsPanel = new EncodedSignalSettingsPanel();
        private InterferenceSettingsPanel _interferenceSettingsPanel = new InterferenceSettingsPanel();
        private TextBox _probabilityErrorResultTextBoxFirst = new TextBox();

        private void ActivateProbabilityErrorCalculationImageButton_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (GeneralChannelConfigurationStackPanel.Visibility == Visibility.Collapsed)
            {
                GeneralChannelConfigurationStackPanel.Visibility = Visibility.Visible;
                ProbabilityErrorCalulcationStackPanel.Visibility = Visibility.Hidden;
                ProbabilityErrorCalulcationStackPanel.Children.Clear();
            }
            else
            {
                GeneralChannelConfigurationStackPanel.Visibility = Visibility.Collapsed;
                ProbabilityErrorCalulcationStackPanel.Visibility = Visibility.Visible;
                ProbabilityErrorCalulcationStackPanel.Children.Add(_encodedSignalSettingsPanel);
                ProbabilityErrorCalulcationStackPanel.Children.Add(_interferenceSettingsPanel);
                var imageButton = new Image();
                imageButton.Width = 110;
                imageButton.Height = 30;
                imageButton.MouseDown += CalculateProbabilityImageButton_OnMouseDown;
                imageButton.Source = ImageButtonProvider.GetImageBitmapSource("../../ImageResource/Buttons/SaveSignalChangesButton.jpg");
                ProbabilityErrorCalulcationStackPanel.Children.Add(imageButton);


                _probabilityErrorResultTextBoxFirst.Width = 520;
                _probabilityErrorResultTextBoxFirst.IsReadOnly = true;
                ProbabilityErrorCalulcationStackPanel.Children.Add(_probabilityErrorResultTextBoxFirst);
                _probabilityErrorResultTextBoxFirst.Margin = new Thickness(5, 0, 0, 0);

                imageButton.VerticalAlignment = VerticalAlignment.Top;
                imageButton.Margin = new Thickness(-140, 120, 0, 0);
            }
            _encodedSignalSettingsPanel.VerticalAlignment = VerticalAlignment.Top;
            _interferenceSettingsPanel.VerticalAlignment = VerticalAlignment.Top;
            _interferenceSettingsPanel.Margin = new Thickness(0, 5, 0, 0);
        }

        public void CalculateProbabilityImageButton_OnMouseDown(object sender, MouseButtonEventArgs e)
        {

            var encodedSignal = _encodedSignalSettingsPanel.GetSignal() as EncodedSignal;
            var interferenceSignal = _interferenceSettingsPanel.GetSignal() as Interference;
            var composite = new CompositeSignal(0);
            composite.Signals.Add(encodedSignal);
            composite.Signals.Add(interferenceSignal);

            SignalsGraph = SignalsGraphControl;
            _signalGraphPainter = new SignalGraphPainter(FixingTriangleIssueRenderingCheckBox.IsChecked.Value);
            _signalGraphPainter.InitializeGraph(SignalsGraphControl);
            SpectrumGraph = SpectrumGraphControl;
            _spectrumGraphPainter = new SpectrumGraphPainter(FixingTriangleIssueRenderingCheckBox.IsChecked.Value);
            _spectrumGraphPainter.InitializeGraph(SpectrumGraph);

            var color = Color.FromArgb(255, 8, 0);
            _signalGraphPainter.DrawSignal(composite, color);
            _spectrumGraphPainter.DrawSignal(composite, color);


            //double lowLimit = -0.7; //Нижний уровень помехи
            //double highLimit = 0.8; //Верхний уровень помехи
            //double signalHeight = 1; //Высота сигнала, 1 В к примеру
            //double signalZero = 0;
            //double minLevelFor1 = 0.95; //Мин уровень единички, к примеру 0.7 В
            //double maxLevelFor0 = 0.3; //Макс уровень нуля

            if (interferenceSignal == null || encodedSignal == null)
            {
                _probabilityErrorResultTextBoxFirst.Text = "Произошла ошибка";
                return;
            }

            double lowLimit = interferenceSignal.LowLimit; //Нижний уровень помехи
            double highLimit = interferenceSignal.HighLimit; //Верхний уровень помехи
            double signalHeight = EncodedSignal.Height; //Высота сигнала, 1 В к примеру
            double signalZero = EncodedSignal.ZeroLevel;
            double minLevelFor1 = EncodedSignal.MinLevelFor1; //Мин уровень единички, к примеру 0.7 В
            double maxLevelFor0 = EncodedSignal.MaxLevelFor0; //Макс уровень нуля

            var m0 = signalZero + (lowLimit + highLimit) / 2;
            var m1 = signalHeight + (lowLimit + highLimit) / 2;
            var sd = (highLimit - lowLimit) / (3 + 3); //правило трех сигм

            var prob = new Probabilities();

            var p0 = prob.Fraspr(maxLevelFor0, m0, sd) - prob.Fraspr(signalZero + lowLimit, m0, sd);
            var p1 = prob.Fraspr(signalHeight + highLimit, m1, sd) - prob.Fraspr(minLevelFor1, m1, sd);

            var e0 = 1.0 - p0;
            var e1 = 1.0 - p1;

            var text = "";

            text += "Высота уровня \"1\" сигнала : " + signalHeight + "\n";
            text += "Высота уровня \"0\" сигнала : " + signalZero + "\n";
            text += "Минимальный уровень для \"1\" : " + minLevelFor1 + "\n";
            text += "Максимальный уровень для  \"0\" : " + maxLevelFor0 + "\n";
            text += "\n";

            text += "Среднеквадратичное отклонение: " + sd + "\n";
            text += "Мат. Ожидание для уровня 1 : " + m1 + "\n";
            text += "Мат. Ожидание для уровня 0 : " + m0 + "\n";
            text += "Вероятность ошибки 1->0 : " + e1 + "\n";
            text += "Вероятность ошибки 0->1 : " + e0 + "\n";

            text += "\n";

            var wordProb = 1.0;
            //Вероятность ошибки в кодовом слове
            foreach (bool el in encodedSignal.Code)
            {
                if (el)
                {
                    wordProb *= p1;
                }
                else
                {
                    wordProb *= p0;
                }
            }

            var wordEr = 1.0 - wordProb;

            text += "Вероятность передачи слова без ошибок : " + wordProb + "\n";
            text += "Вероятность 1 и более ошибки при передаче слова : " + wordEr + "\n";

            _probabilityErrorResultTextBoxFirst.Text = text;
        }
    }
}
