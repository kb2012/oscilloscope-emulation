﻿using System.Windows;

namespace OscilloscopeEmulation
{
    /// <summary>
    /// Interaction logic for LogoAnimationWindow.xaml
    /// </summary>
    public partial class LogoAnimationWindow : Window
    {
        public LogoAnimationWindow()
        {
            InitializeComponent();
        }

        public void ShowLogo(Window generalWindow)
        {
            var animation = new Animation(@"..\..\ImageResource\LogoAnimation", LogoAnimation, this, generalWindow);
            animation.Animate();
        }
    }
}
