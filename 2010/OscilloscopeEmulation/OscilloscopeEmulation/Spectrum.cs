﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using OscilloscopeEmulation.SpectrumFunctions;
using Window = OscilloscopeEmulation.SpectrumFunctions.Window;

namespace OscilloscopeEmulation
{
    public class Spectrum
    {
        private bool _useWindowFunction = false;
        public IList<double> GetSpectrumFromList(IList<double> values)
        {
            double windowCoeff = 1.0;
            if (_useWindowFunction)
            {
                var count = values.Count;
                //Оконная функция для увеличения точности
                for (var i = 0; i < count; i++)
                {
                    values[i] *= Window.BlackmannHarris(i, count);
                }

                //todo возможно стоит изменить на более точный способ определения
                windowCoeff = 100000000/35839131.0; //Коэффициент сжатия оконной функции, посчитан эксперементально
            }
            var complexArray = GetComplexArray(values);

            var complexSpectrum = FFT.Nfft(FFT.Fft(complexArray)); //Fft делает односторонний спектр, Nfft переворачивает до двухстороннего
            //Формулы для магнитуды и фазы, первую используем для построения спектра
            //magnitude = Math.Sqrt(x.Real*x.Real + x.Imaginary*x.Imaginary)
            //phase = Math.Atan2(x.Imaginary, x.Real)
            var spectrum = complexSpectrum.Select(d => windowCoeff*Math.Sqrt(d.Real * d.Real + d.Imaginary * d.Imaginary)).ToArray();

            return spectrum;

        }

        private Complex[] GetComplexArray(IList<double> values)
        {
            var count = values.Count;
            var powerOfTwo = (int) Math.Pow(2, Math.Ceiling(Math.Log(count, 2)));

            var complexArray = new Complex[powerOfTwo];
            var i = 0;
            foreach (var value in values)
            {
                var complex = new Complex(value, 0);
                complexArray[i] = complex;
                i++;
            }
            for (var j = count; j < powerOfTwo; j++)
            {
                complexArray[j] = new Complex(0, 0);
            }
            return complexArray;
        }
    }
}
