﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using OscilloscopeEmulation.Signals;
using OscilloscopeEmulation.SignalSettingsPanels;
using System.Globalization;

namespace OscilloscopeEmulation.ChannelPanels
{
    /// <summary>
    /// Interaction logic for ChannelItem.xaml
    /// </summary>
    public partial class ChannelItem
    {
        public CompositeSignal CurrentCompositeSignal;
        public ChannelConfigurationStackPanel ChannelConfigurationStackPanel { get; set; }
        public LowPassFilterSettingsPanel LowPassFilterSettingsPanel { get; set; }
        public SampledSignalSettingsPanel SampledSignalSettingsPanel { get; set; }
        public string ChannelItemName { get; set; }
        private int _previousChannelNumber = -1;

        public ChannelItem(string channelName, ChannelConfigurationStackPanel channelConfigurationStackPanel, Color color)
        {
            InitializeComponent();
            CurrentCompositeSignal = new CompositeSignal(int.Parse(channelName.Split('-')[1], CultureInfo.InvariantCulture));
            ChannelNameLabel.Content = ChannelItemName = channelName;
            ActivatingChannelCheckBox.IsChecked = true;
            ChannelConfigurationStackPanel = channelConfigurationStackPanel;
            ChannelColorPicker.SelectedColor = color;
            LowPassFilterSettingsPanel = new LowPassFilterSettingsPanel(CurrentCompositeSignal);
            SampledSignalSettingsPanel = new SampledSignalSettingsPanel(CurrentCompositeSignal);
            EditingChannelImageButton.Source =
                ImageButtonProvider.GetImageBitmapSource(@"..\..\ImageResource\Buttons\EditIcon.png");
            ChannelItemStackPanel.Background = new ImageBrush
            {
                ImageSource =
                  new BitmapImage(
                    new Uri(@"..\..\ImageResource\Buttons\ChannelItemBackground.png", UriKind.Relative)
                  )
            };
        }

        public ISignal GetResultingSignal()
        {
            var channelItem = this;
            var selectedColor = channelItem.ChannelColorPicker.SelectedColor;
            var currentColor = System.Drawing.Color.FromArgb(selectedColor.R, selectedColor.G, selectedColor.B);

            if (channelItem.EnableSamplingCheckBox.IsChecked.Value &&
                channelItem.EnableLowPassFilterCheckBox.IsChecked.Value)
            {
                if (channelItem.SampledSignalSettingsPanel.ResultingDiscretizedSignalSignal != null &&
                    channelItem.LowPassFilterSettingsPanel.ResultingLowFilteredSignal != null)
                {
                    var discretizationFrequency =
                        channelItem.SampledSignalSettingsPanel.ResultingDiscretizedSignalSignal
                            .DiscretizationFrequency;
                    var discretizedSignal = new DiscretizedSignal(channelItem.CurrentCompositeSignal,
                        discretizationFrequency);
                    var low = channelItem.LowPassFilterSettingsPanel.ResultingLowFilteredSignal;
                    var lowFilteredSignal = new LowFilteredSignal(discretizedSignal, low.N, low.Fd, low.Fs, low.Fx);
                    return lowFilteredSignal;
                }
                else
                {
                    return channelItem.CurrentCompositeSignal;
                }
            }
            else if (channelItem.EnableSamplingCheckBox.IsChecked.Value)
            {
                if (channelItem.SampledSignalSettingsPanel.ResultingDiscretizedSignalSignal != null)
                {
                    return channelItem.SampledSignalSettingsPanel.ResultingDiscretizedSignalSignal;
                }
                else
                {
                    channelItem.EnableSamplingCheckBox.IsChecked = false;
                }
            }
            else if (channelItem.EnableLowPassFilterCheckBox.IsChecked.Value)
            {
                if (channelItem.LowPassFilterSettingsPanel.ResultingLowFilteredSignal != null)
                {
                    return channelItem.LowPassFilterSettingsPanel.ResultingLowFilteredSignal;
                }
                else
                {
                    channelItem.EnableLowPassFilterCheckBox.IsChecked = false;
                }
            }
            return channelItem.CurrentCompositeSignal;
        }

        private void EditingChannelImageButton_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            RefreshSignalComponentVisualization();
            ChannelConfigurationStackPanel.SignalItemsGroupBox.Header = string.Format(
                "Сигналы выбранного канала '{0}'", ChannelNameLabel.Content);
            //ChannelConfigurationStackPanel.SignalItemsStackPanel.Children.Clear();
            ChannelConfigurationStackPanel.ConfigurationCurrentSignalStackPanel.Children.Clear();
        }

        public void RefreshSignalComponentVisualization()
        {
            ChannelConfigurationStackPanel.LowPassFilterAndSampledSignalGroupBox.Visibility = Visibility.Hidden;
            ChannelConfigurationStackPanel.SignalItemsStackPanel.Children.Clear();
            foreach (var signal in CurrentCompositeSignal.Signals)
            {
                ChannelConfigurationStackPanel.SignalItemsStackPanel.Children.Add(new SignalItem(signal,
                    ChannelConfigurationStackPanel, CurrentCompositeSignal, this));
            }
            ChannelConfigurationStackPanel.AddSignalToChannelImageButton.Visibility = Visibility.Visible;
            ChannelConfigurationStackPanel.AddSignalToChannelImageButton.MouseDown += AddingSignalButtonOnClick;
            ChannelConfigurationStackPanel.AddSignalToChannelImageButton.Source =
                ImageButtonProvider.GetImageBitmapSource(@"../../ImageResource/Buttons/ChooseSignalForCurrentChannelButton.jpg");
        }

        private void AddingSignalButtonOnClick(object sender, RoutedEventArgs routedEventArgs)
        {
            ChannelConfigurationStackPanel.ConfigurationCurrentSignalStackPanel.Children.Clear();
            ChannelConfigurationStackPanel.ConfigurationCurrentSignalStackPanel.Children.Add(new SignalSelectorPanel(CurrentCompositeSignal, this));
        }

        private void EnableLowPassFilterCheckBox_OnChecked(object sender, RoutedEventArgs e)
        {
            ChannelConfigurationStackPanel.LowPassFilterAndSampledSignalGroupBox.Visibility = Visibility.Visible;
            ChannelConfigurationStackPanel.ConfigurationCurrentSignalStackPanel.Children.Clear();
            var configurationChildren = ChannelConfigurationStackPanel.LowPassFilterAndSampledSignalStackPanel.Children;
            configurationChildren.Clear();
            if (configurationChildren.Count == 0)
                ChannelConfigurationStackPanel.LowPassFilterAndSampledSignalStackPanel.Children.Add(LowPassFilterSettingsPanel);
        }

        private void EnableLowPassFilterCheckBox_OnUnchecked(object sender, RoutedEventArgs e)
        {
            ChannelConfigurationStackPanel.LowPassFilterAndSampledSignalStackPanel.Children.Clear();
            ChannelConfigurationStackPanel.LowPassFilterAndSampledSignalGroupBox.Visibility = Visibility.Hidden;
        }

        private void EnableSamplingCheckBox_OnChecked(object sender, RoutedEventArgs e)
        {
            ChannelConfigurationStackPanel.LowPassFilterAndSampledSignalGroupBox.Visibility = Visibility.Visible;
            ChannelConfigurationStackPanel.ConfigurationCurrentSignalStackPanel.Children.Clear();
            var configurationChildren = ChannelConfigurationStackPanel.LowPassFilterAndSampledSignalStackPanel.Children;
            configurationChildren.Clear();
            if (configurationChildren.Count == 0)
                ChannelConfigurationStackPanel.LowPassFilterAndSampledSignalStackPanel.Children.Add(SampledSignalSettingsPanel);
        }

        private void EnableSamplingCheckBox_OnUnchecked(object sender, RoutedEventArgs e)
        {
            ChannelConfigurationStackPanel.LowPassFilterAndSampledSignalStackPanel.Children.Clear();
            ChannelConfigurationStackPanel.LowPassFilterAndSampledSignalGroupBox.Visibility = Visibility.Hidden;
        }

        private void InputChannelsComboBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_previousChannelNumber != -1)
            {
                CurrentCompositeSignal.Signals.Remove(ChannelConfigurationStackPanel.ChannelItems[_previousChannelNumber].GetResultingSignal());
            }
            if (e.AddedItems.Count > 0)
            {
                var comboBoxItem = e.AddedItems[0] as ComboBoxItem;
                var number = int.Parse(comboBoxItem.Content.ToString().Split('-')[1], CultureInfo.InvariantCulture);
                EditingChannelImageButton_OnMouseDown(null, null);
                CurrentCompositeSignal.Add(ChannelConfigurationStackPanel.ChannelItems[number].GetResultingSignal());
                _previousChannelNumber = number;
            }
        }
    }
}
