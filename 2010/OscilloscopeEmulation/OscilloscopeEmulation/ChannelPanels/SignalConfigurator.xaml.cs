﻿using System;
using System.Windows;
using System.Windows.Controls;
using OscilloscopeEmulation.Signals;
using OscilloscopeEmulation.SignalSettingsPanels;

namespace OscilloscopeEmulation.ChannelPanels
{
    /// <summary>
    /// Interaction logic for SignalConfigurator.xaml
    /// </summary>
    public partial class SignalConfigurator : UserControl
    {
        public ISignal Signal { get; set; }
        public ISignalSettingsPanel SettingsPanel { get; set; }
        public CompositeSignal CompositeChannelSignal { get; set; }
        public ChannelItem ChannelItem { get; set; }
        public ChannelConfigurationStackPanel ChannelConfigurationStackPanel { get; set; }
        public SignalConfigurator(ISignal signal, CompositeSignal compositeChannelSignal, ChannelItem channelItem, ChannelConfigurationStackPanel channelConfigurationStackPanel)
        {
            InitializeComponent();
            Signal = signal;
            CompositeChannelSignal = compositeChannelSignal;
            ChannelItem = channelItem;
            ChannelConfigurationStackPanel = channelConfigurationStackPanel;
            SettingsPanel = signal.GetSignalSettingsPanel();
            SaveChangesButton.Source =
                ImageButtonProvider.GetImageBitmapSource(
                    @"../../ImageResource/Buttons/SaveSignalChangesButton.jpg");
            RemoveSignalFromChannel.Source =
                ImageButtonProvider.GetImageBitmapSource(
                    @"../../ImageResource/Buttons/RemoveSignalFromChannelButton.jpg");
            SelectedSignalSettingsStackPanel.Children.Add(SettingsPanel as UserControl);
        }

        private void SaveChangesButton_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                Signal.CloneSignal(SettingsPanel.GetSignal());
            }
            catch (Exception)
            {
                MessageBox.Show("Не указаны параметры для изменения настроек сигнала!");
            }
            ChannelItem.RefreshSignalComponentVisualization();
            SelectedSignalSettingsStackPanel.Children.Clear();
            ChannelConfigurationStackPanel.ConfigurationCurrentSignalStackPanel.Children.Clear();
        }

        private void RemoveSignalFromChannel_OnClick(object sender, RoutedEventArgs e)
        {
            CompositeChannelSignal.Signals.Remove(Signal);
            ChannelItem.RefreshSignalComponentVisualization();
            SelectedSignalSettingsStackPanel.Children.Clear();
            ChannelConfigurationStackPanel.ConfigurationCurrentSignalStackPanel.Children.Clear();
        }
    }
}
