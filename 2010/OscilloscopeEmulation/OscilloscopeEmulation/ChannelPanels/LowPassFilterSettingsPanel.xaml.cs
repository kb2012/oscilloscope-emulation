﻿using System.Windows.Controls;
using System.Windows.Input;
using OscilloscopeEmulation.Signals;
using System.Globalization;

namespace OscilloscopeEmulation.ChannelPanels
{
    /// <summary>
    /// Interaction logic for LowPassFilterSettingsPanel.xaml
    /// </summary>
    public partial class LowPassFilterSettingsPanel
    {
        private CompositeSignal _compositeChannelSignal;
        public LowFilteredSignal ResultingLowFilteredSignal;

        public LowPassFilterSettingsPanel(CompositeSignal compositeChannelSignal)
        {
            InitializeComponent();
            _compositeChannelSignal = compositeChannelSignal;
            SaveLowFilterPassSettingsImageButton.Source =
                ImageButtonProvider.GetImageBitmapSource(@"..\..\ImageResource\Buttons\SaveSignalChangesButton.jpg");
        }

        private void SaveLowFilterPassSettingsImageButton_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            ResultingLowFilteredSignal = new LowFilteredSignal(_compositeChannelSignal, int.Parse(FilterLengthTextBox.Text, CultureInfo.InvariantCulture),
                double.Parse(SamplingFrequencyTextBox.Text, CultureInfo.InvariantCulture), double.Parse(FrequencyBandwidthTextBox.Text, CultureInfo.InvariantCulture), double.Parse(FrequencyBandAttenuationTextBox.Text, CultureInfo.InvariantCulture));
        }
    }
}
