﻿using System;
using System.Drawing;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using OscilloscopeEmulation.Signals;

namespace OscilloscopeEmulation.ChannelPanels
{
    /// <summary>
    /// Interaction logic for SignalItem.xaml
    /// </summary>
    public partial class SignalItem : UserControl
    {
        public ISignal Signal { get; set; }
        public ChannelConfigurationStackPanel ChannelConfigurationStackPanel;
        public CompositeSignal _channelCompositeSignal;
        public ChannelItem ChannelItem;
        private SignalConfigurator _signalConfigurator;
        public SignalItem(ISignal signal, ChannelConfigurationStackPanel channelConfigurationStackPanel, CompositeSignal channelCompositeSignal, ChannelItem channelItem)
        {
            InitializeComponent();
            Signal = signal;
            _channelCompositeSignal = channelCompositeSignal;
            ChannelItem = channelItem;
            SignalNameTextBlock.Text = signal.ToString();
            ChannelConfigurationStackPanel = channelConfigurationStackPanel;
            SignalItemStackPanel.Background = new ImageBrush
            {
                ImageSource =
                  new BitmapImage(
                    new Uri(@"..\..\ImageResource\Buttons\SignalItemBackground.png", UriKind.Relative)
                  )
            };
            EditingSignalImageButton.Source =
                ImageButtonProvider.GetImageBitmapSource(@"..\..\ImageResource\Buttons\EditIcon.png");
        }

        private void UIElement_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            if(_signalConfigurator == null)
                _signalConfigurator = new SignalConfigurator(Signal, _channelCompositeSignal, ChannelItem, ChannelConfigurationStackPanel);
            ChannelConfigurationStackPanel.ConfigurationCurrentSignalStackPanel.Children.Clear();
            ChannelConfigurationStackPanel.ConfigurationCurrentSignalStackPanel.Children.Add(_signalConfigurator);
        }

        private void ActivatingSignalCheckBox_OnChecked(object sender, RoutedEventArgs e)
        {
            var children = ChannelConfigurationStackPanel.SignalItemsStackPanel.Children;
            foreach (var child in children)
            {
                if (child is SignalItem)
                {
                    if (!Equals(child as SignalItem))
                        (child as SignalItem).ActivatingAmplitudeModulationCheckBox.IsChecked = false;
                }
            }
        }
    }
}
