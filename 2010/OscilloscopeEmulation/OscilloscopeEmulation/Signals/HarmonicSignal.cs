﻿using System;
using System.Collections.Generic;
using OscilloscopeEmulation.SignalSettingsPanels;

namespace OscilloscopeEmulation.Signals
{
    public class HarmonicSignal:ISignal
    {
        public double Amplitude { get; private set; }
        public double Frequency { get; private set; }
        public double StartPhase { get; private set; }
        public ISignalSettingsPanel SignalSettingsPanel { get; set; }

        public HarmonicSignal(double amplitude, double frequency, double startPhase = 0)
        {
            //частота в кГц, амплитуда в вольтах, старт. фаза по идее всегда 0 должна быть у нас, но на всякий случай добавим параметр
            Amplitude = amplitude;
            Frequency = frequency;
            StartPhase = startPhase;
        }

        public IList<double> GetValuesList(int totalMSec, int totalEntries) //10 мсек и 1000 по умолчанию
        {
            var list = new List<double>();
            for (var i = 0; i < totalEntries; i++)
            {
                list.Add(GetValue(i, totalMSec, totalEntries));
            }
            return list;
        }

        public double GetValue(int i, int totalMSec, int totalEntries)
        {
            //A*cos(2*Pi*f*t+phase0), t = i/TotalMSec
            //Всего 10 мсек, когда i = 1000 t = 10. В остальном кГц компенсируют миллисекунды (Умножили и поделили на 1000)
            var scaleFactor = (double)totalMSec / totalEntries;
            var wt = 2 * Math.PI * Frequency * i * scaleFactor; //A*Cos(2*pi*f*t + phase0) = A*cos(w*t + phase0)
           return (Amplitude * Math.Cos(wt + StartPhase));
        }

        public string GetName()
        {
            return string.Format("Гармонический, А={0} В, {1} кГц", Amplitude, Frequency);
        }

        public override string ToString()
        {
            return string.Format("Гармонический [Амплитуда: '{0} В'; Частота: '{1} кГц']", Amplitude, Frequency);
        }

        public ISignalSettingsPanel GetSignalSettingsPanel()
        {
            if (SignalSettingsPanel == null)
                SignalSettingsPanel = new HarmonicSignalSettingsPanel();
            return SignalSettingsPanel;
        }

        public void CloneSignal(ISignal signal)
        {
            Amplitude = (signal as HarmonicSignal).Amplitude;
            Frequency = (signal as HarmonicSignal).Frequency;
            StartPhase = (signal as HarmonicSignal).StartPhase;
        }
    }
}
