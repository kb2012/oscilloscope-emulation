﻿using System.Collections.Generic;

namespace OscilloscopeEmulation.Signals
{
    public class SampledSignal : ISignal
    {
        public ISignal SourceSignal { get; set; }
        public int SamplingRate { get; set; }

        public IList<double> GetValuesList(int totalMSec, int totalEntries)
        {
            var list = new List<double>();
            for (var i = 0; i < totalEntries; i++)
            {
                list.Add(GetValue(i, totalMSec, totalEntries));
            }
            return list;
        }

        public double GetValue(int i, int totalMSec, int totalEntries)
        {
            if (i%SamplingRate == 0)
                return SourceSignal.GetValue(i, totalMSec, totalEntries);

            return 0;
        }

        public string GetName()
        {
            return "";
        }
    }
}
