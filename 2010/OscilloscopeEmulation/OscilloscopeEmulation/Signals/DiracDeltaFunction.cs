﻿using System.Collections.Generic;
using OscilloscopeEmulation.SignalSettingsPanels;

namespace OscilloscopeEmulation.Signals
{
    public class DiracDeltaFunction : ISignal
    {
        public static double Height = 1.0; //Высота дельта-функции
        public ISignalSettingsPanel SignalSettingsPanel { get; set; }

        public IList<double> GetValuesList(int totalMSec, int totalEntries)
        {
            var list = new List<double>();
            for (var i = 0; i < totalEntries; i++)
            {
                list.Add(GetValue(i, totalMSec, totalEntries));
            }
            return list;
        }

        public double GetValue(int i, int totalMSec, int totalEntries)
        {
            if (totalEntries/2 - 1 < i && totalEntries/2 + 2 > i)
                return Height;

            return 0;
        }

        public string GetName()
        {
            return "Сигнал Дирака";
        }

        public override string ToString()
        {
            return string.Format("Дельта-функция Дирака");
        }

        public ISignalSettingsPanel GetSignalSettingsPanel()
        {
            if (SignalSettingsPanel == null)
                SignalSettingsPanel = new ConstantSignalSettingsPanel();
            return SignalSettingsPanel;
        }

        public void CloneSignal(ISignal signal)
        {
            
        }
    }
}
