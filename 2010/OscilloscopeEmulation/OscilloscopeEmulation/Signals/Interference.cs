﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OscilloscopeEmulation.SignalSettingsPanels;

namespace OscilloscopeEmulation.Signals
{
    public class Gauss
    {
        bool ready = false;
        double second = 0.0;
        private static Random rand = new Random();

        /// <summary>
        ///  Генерация пары значений по закону Гаусса
        /// </summary>
        /// <param name="mean">математическое ожидание</param>
        /// <param name="dev">среднеквадратическое отклонение</param>
        /// <returns>Первое значение  из пары сгенерированных, а потом второе</returns>
        public double Next(double mean, double dev)
        {
            if (this.ready)
            {
                this.ready = false;
                return this.second * dev + mean;
            }
            else
            {
                double u, v, s;

                do
                {
                    u = 2.0 * rand.NextDouble() - 1.0;
                    v = 2.0 * rand.NextDouble() - 1.0;
                    s = u * u + v * v;
                } while (s >= 1.0 || Math.Abs(s) < 0.001);

                var r = Math.Sqrt(-2.0 * Math.Log(s) / s);
                this.second = r * u;
                this.ready = true;
                return (r * v * dev + mean);
            }
        }
        public double Next()
        {
            return this.Next(0.0, 1.0);
        }

    }
    public class Interference: ISignal
    {
        public double LowLimit{get;private set;}
        public double HighLimit{get;private set;}
        public ISignalSettingsPanel SignalSettingsPanel { get; set; }

        public Interference(double highLimit) 
        {
            LowLimit = 0;
            HighLimit = highLimit;
        }

        public Interference(double lowLimit, double highLimit)
            : this(highLimit)
        {
            LowLimit = lowLimit;
        }

        public IList<double> GetValuesList(int totalMSec, int totalEntries)
        {
            var list = new List<double>();
            for (int i = 0; i < totalEntries; i++)
            {
                list.Add(this.GetValue(i, totalMSec, totalEntries));
            }
            return list;
        }

        public double GetValue(int i, int totalMSec, int totalEntries)
        {
            Gauss r = new Gauss();
            double randNormal = LowLimit - 0.01;
            while (randNormal > HighLimit || randNormal < LowLimit)
            {
                randNormal = r.Next(0, 1);
            }
            return randNormal;
        }

        public string GetName()
        {
            return string.Format("Интерференция, нижний предел = {0}, верхний предел = {1}", LowLimit, HighLimit);
        }

        public override string ToString()
        {
            return string.Format("Помеха [Макс. уровень помехи: '{0} В'; Мин. уровень помехи: '{1} В']", HighLimit, LowLimit);
        }

        public ISignalSettingsPanel GetSignalSettingsPanel()
        {
            if (SignalSettingsPanel == null)
                SignalSettingsPanel = new InterferenceSettingsPanel();
            return SignalSettingsPanel;
        }

        public void CloneSignal(ISignal signal)
        {
            HighLimit = (signal as Interference).HighLimit;
            LowLimit = (signal as Interference).LowLimit;
        }
    }
}
