﻿using System.Collections.Generic;
using System.Linq;
using OscilloscopeEmulation.SignalSettingsPanels;

namespace OscilloscopeEmulation.Signals
{
    public class EncodedSignal : ISignal
    {
        public bool[] Code { get; private set; }
        private int _length;

        public static double Period = 1.0; //Период в мсек
        public static double Height = 1.0; //Высота уровня 1 сигнала в вольтах
        public static double ZeroLevel = 0; //Высота уровня 0

        public static double MinLevelFor1 = 0.7; // Минимальный уровень распознования как 1 для подсчета ошибок
        public static double MaxLevelFor0 = 0.3; // Максимальный уровень распознования как 0 для подсчета ошибок
        private double[] _intervals; //Интервалы, служебная переменная
        public ISignalSettingsPanel SignalSettingsPanel { get; set; }

        //Из гуя думаю лучше вызывать этот конструктор, тот, что ниже - больше для теста
        public EncodedSignal(bool[] code)
        {
            Code = code;
            _length = code.Length;
        }

        //Строковый конструктор вида "10101" без пробелов и чего-либо лишнего, использовать скорее всего только для теста
        public EncodedSignal(string code)
        {
            _length = code.Length;
            Code = new bool[_length];
            for (var i = 0; i < _length; i++)
            {
                if (code[i] == '1')
                {
                    Code[i] = true;
                }
                else
                {
                    Code[i] = false;
                }
            }
        }

        private int GetInterval(double msecPassed)
        {
            var modMsec = msecPassed;
            //Цикл эмулирует mod (деление с остатком) для double
            while (modMsec >= Period)
            {
                modMsec -= Period;
            }

            //Нормируем
            modMsec *= Period;
            var interval = (int)(modMsec * _length);
            if (interval >= _length) interval = 0;
            return interval;
        }

        public IList<double> GetValuesList(int totalMSec, int totalEntries)
        {
            var list = new List<double>();
            for (var i = 0; i < totalEntries; i++)
            {
                list.Add(GetValue(i, totalMSec, totalEntries));
            }
            return list;
        }

        public double GetValue(int i, int totalMSec, int totalEntries)
        {
            var msecPassed = i * (double)totalMSec / totalEntries;

            if (Code[GetInterval(msecPassed)])
                return Height;

            return ZeroLevel;
        }

        public string GetName()
        {
            return "Кодированный сигнал " + string.Join(" ", Code.Select(code => code == true ? 1 : 0));
        }

        public override string ToString()
        {
            return string.Format("Кодированный сигнал [Код: '{0}'']", string.Join("", Code.Select(code => code ? 1 : 0)));
        }

        public ISignalSettingsPanel GetSignalSettingsPanel()
        {
            if (SignalSettingsPanel == null)
                SignalSettingsPanel = new EncodedSignalSettingsPanel();
            return SignalSettingsPanel;
        }

        public void CloneSignal(ISignal signal)
        {
            Code = (signal as EncodedSignal).Code;
        }
    }
}
